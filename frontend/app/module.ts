import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { Routing } from './routing';
import { AppComponent } from './compopnents/app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
  	NgbModule,
    BrowserModule,
	  Routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class Module { }
